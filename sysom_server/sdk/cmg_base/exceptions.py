# -*- coding: utf-8 -*- #
"""
Time                2023/3/17 18:46
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                exceptions.py
Description:
"""


class CmgException(Exception):
    """CMG base exception

    This class defines the base exception for CMG, and all exceptions thrown
    by CMG should inherit from this class.
    """


class CmgProtoAlreadyExistsException(CmgException):
    """Exceptions thrown for duplicate proto(submodule) registration

    This exception should be thrown if the proto(submodule) already exists while
    registering a submodule.
    """


class CmgProtoNotExistsException(CmgException):
    """Exceptions thrown for trying to use a non-existent proto(submodule)

    Exceptions that will be thrown when trying to use a non-existent proto
    (submodule).
    """


class CmgNotValidCmgUrlException(CmgException):
    """Exception thrown when an invalid CmgUrl format is parsed."""
