import unittest
from cmg_base.service_check import HTTPHealthCheck, ServiceCheck, HealthState


class MyTestCase(unittest.TestCase):

    def test_http_health_check_offline(self):
        http_health_check = HTTPHealthCheck(
            ServiceCheck.http("https://127.0.0.1:7886", 2, 5)
        )
        self.assertEqual(http_health_check.check(), HealthState.OFFLINE)

    def test_http_health_check_online(self):
        http_health_check = HTTPHealthCheck(
            ServiceCheck.http("https://www.baidu.com", 2, 5)
        )
        self.assertEqual(http_health_check.check(), HealthState.ONLINE)


if __name__ == '__main__':
    unittest.main()
