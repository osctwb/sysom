#!/bin/bash -x

RESOURCE_DIR=${NODE_HOME}/${SERVICE_NAME}
SYSAK_VER=1.3.0-2
SYSAK_ARCH=x86_64
SYSAK_PKG=sysak-${SYSAK_VER}.${SYSAK_ARCH}.rpm

main()
{
    rpm -qa | grep sysak
    if [ $? -eq 0 ]
    then
        exit 0
    fi

    ###install sysak from local yum repo first###
    yum install -y sysak
    if [ $? -eq 0 ]
    then
        exit 0
    fi

    yum install -y ${SYSAK_PKG}
    exit $?
}

main
