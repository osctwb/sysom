import ProForm, { ProFormDigit, ProFormText, ProFormSelect } from '@ant-design/pro-form';
import { Button } from 'antd';
import ProCard from '@ant-design/pro-card';
import { useRequest, FormattedMessage, useIntl } from 'umi';
import { postTask, queryHost } from '../service';

const TaskFrom = (props) => {
  const taskForm = props.taskForm
  const serviceName = props.serviceName
  const intl = useIntl();


  const { loading, error, run } = useRequest(postTask, {
    manual: true,
    onSuccess: (result, params) => {
      props?.onSuccess?.(result, params)
    },
  });

  return (
    <ProCard>
      <ProForm
        onFinish={async (values) => {
          values.instance = values.instance[0]
          run(values) 
        }}
        submitter={{
          submitButtonProps: { style: { display: 'none' } },
          resetButtonProps: { style: { display: 'none' } }
        }}
        layout={"horizontal"}
        autoFocusFirstInput
      >
        <ProFormText
          name={"service_name"}
          initialValue={serviceName}
          hidden={true}
        />
        <ProForm.Group>
          {
            taskForm?.map(formItem => {

              if (formItem.type == "text") {
                return (< ProFormText
                  key={formItem.name}
                  name={formItem.name}
                  label={formItem.label}
                  initialValue={formItem.initialValue}
                  tooltip={formItem.tooltips}
                />);
              } else if (formItem.type == "digit") {
                return (< ProFormDigit
                  key={formItem.name}
                  name={formItem.name}
                  label={formItem.label}
                  initialValue={formItem.initialValue}
                  tooltip={formItem.tooltips}
                />);
              } else if (formItem.type == "select") {
                return (
                  < ProFormSelect
                    key={formItem.name}
                    name={formItem.name}
                    label={formItem.label}
                    initialValue={formItem.initialValue}
                    tooltip={formItem.tooltips}
                    options={formItem.options}
                  />);
              } else if (formItem.type == "select_host") {
                return (< ProFormSelect
                    name={formItem.name}
                    label={formItem.label}
                    tooltip={formItem.tooltips}
                    width="sm"
                    placeholder="请选择实例IP"
                    mode='tags'
                    request={async ({ keyWords = '' }) => {
                      const { data } = await queryHost()
                      let hostArr = []
                      data.map((item)=> {
                          hostArr.push({label: item.ip, value: item.ip})
                      })
                      return hostArr.filter(({ value, label }) => {
                          return value.includes(keyWords) || label.includes(keyWords);
                      });
                    }}
                    rules={[
                      { required: true, message: 'Please input your instance!' },
                    ]}
                 />)

              }
            })
          }
          <Button type="primary" htmlType="submit" loading={loading}><FormattedMessage id="pages.diagnose.startdiagnosis" defaultMessage="Start diagnosis" /></Button>
          <Button type="primary" loading={loading} onClick={() => {
            props?.onOfflineLoad?.()
          }}>
            {
              intl.formatMessage({
                id: 'pages.diagnose.offline_import.btn',
                defaultMessage: 'Offline import',
              })
            }
          </Button>
        </ProForm.Group>
      </ProForm>
    </ProCard>
  )
}

export default TaskFrom
