import ProForm, { ProFormText, ProFormSelect } from '@ant-design/pro-form';
import { Button } from 'antd';
import { useRequest, useIntl, FormattedMessage } from 'umi';
import ProCard from '@ant-design/pro-card';
import { postTask } from '../service'
import { getHost } from '@/pages/host/service'

export default (props) => {
    const intl = useIntl();
    const { loading, error, run } = useRequest(postTask, {
        manual: true,
        onSuccess: (result, params) => {
            props?.onSuccess?.(result, params);
        },
    });

    return (

        <ProCard>
            <ProForm
                onFinish={async (values) => {
                    values.instance = values.instance[0]
                    run(values)
                }}
                submitter={{
                    submitButtonProps: {
                        style: {
                            display: 'none',
                        },
                    },
                    resetButtonProps: {
                        style: {
                            display: 'none',
                        },
                    },
                }}
                layout={"horizontal"}
                autoFocusFirstInput
            >
                <ProFormText
                    name={"service_name"}
                    initialValue={"ossre"}
                    hidden={true}
                />
                <ProForm.Group>
                    <ProFormSelect
                        name={"instance"}
                        width="md"
                        mode='tags'
                        label={intl.formatMessage({
                            id: 'pages.diagnose.instanceIP',
                            defaultMessage: 'Instance IP',
                        })}
                        request={async ({ keyWords = '' }) => {
                            const { data } = await getHost()
                            let hostArr = []
                            data.map((item)=> {
                                hostArr.push({label: item.ip, value: item.ip})
                            })
                            return hostArr.filter(({ value, label }) => {
                                return value.includes(keyWords) || label.includes(keyWords);
                            });
                        }}
                        rules={[
                            { required: true, message: 'Please input your instance!' },
                        ]}
                    />
                    <Button type="primary" htmlType="submit" loading={loading}><FormattedMessage id="pages.diagnose.startdiagnosis" defaultMessage="Start diagnosis" /></Button>
                </ProForm.Group>
            </ProForm>
        </ProCard>
    )
}
